#include "DigitalClock.h"

#include <QPainter>
#include <QTime>
#include <QTimer>
namespace custom {

DigitalClock::DigitalClock(QWidget *parent) : QWidget(parent)
{
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, [=]() {
        update();
    });
    timer->start(1000);
    setWindowTitle(tr("Digital Clock"));
    resize(200, 200);
    setVisible(true);
    setGeometry(100, 100, 200, 200);
}

void DigitalClock::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event)
    QTime time = QTime::currentTime();

    QPainter painter(this);
    painter.drawText(width() / 2, height() / 2, time.toString());
    painter.save();
    painter.restore();
}

}