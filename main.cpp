#include "DigitalClock.h"

#include <QApplication>
#include <QWidget>
#include <QVBoxLayout>

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    QWidget mainWindow;
    mainWindow.setWindowTitle("Simple Qt Widget App");
    mainWindow.setMinimumSize(200, 200);

    QVBoxLayout layout(&mainWindow);
    mainWindow.setLayout(&layout);

    custom::DigitalClock digitalClock;
    layout.addWidget(&digitalClock);

    mainWindow.show();

    return app.exec();
}
