#pragma once

#include <QWidget>

namespace custom {

class DigitalClock : public QWidget {
Q_OBJECT

public:
    ~DigitalClock() {};

    DigitalClock(QWidget *parent = 0);

protected:
    virtual void paintEvent(QPaintEvent *event);
};

}