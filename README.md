### Python binding of custom cpp Qt widget using sip and cmake.

Done from Ubuntu 22.04. The code should be adaptable to windows or macos.

Any improvement or recommendation are welcome (open an issue).

#### requirements

Need these packages (some are perhaps not necessary):
```
sudo apt install build-essential qtbase5-dev python3-dev sip-dev python3-pyqt5 \
 pyqt5-dev qtbase5-dev python3-pyqtbuild python3-sipbuild sip-tools
```

#### build project:
```
SRC_DIR=$PWD
BUILD_DIR="$SRC_DIR/build"
cd $BUILD_DIR
cmake $SRC_DIR # -DQt5_DIR:PATH=$HOME/Qt/5.15.8/gcc_64/lib/cmake/Qt5
make
```

#### Launch the CPP app that uses the custom cpp Qt widget directly:
```
./custom_cpp_widget_app
```

#### Launch the PyQt5 app that uses the custom cpp Qt widget through sip binding:
```
python3 main.py
```